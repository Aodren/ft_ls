# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: abary <marvin@42.fr>                       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/11/23 12:31:57 by abary             #+#    #+#              #
#    Updated: 2016/02/17 20:13:43 by abary            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = ft_ls

INC_DIR = includes

LIB_DIR = libft

INC_LIB_DIR = $(LIB_DIR)/$(INC_DIR)

NAME_LIB = ft_ls.a\

CFLAGS = -Wall -Werror -Wextra -I$(INC_DIR) -I $(INC_LIB_DIR)

SRC = ft_ls.c\
	  main.c\
	  ft_valid_options.c\
	  ft_create_options.c\
	  ft_display_valid_options.c\
	  ft_read_dir.c\
	  ft_ls_erreur.c\
	  ft_lst__newdir.c\
	  ft_lst_addtri_char.c\
	  ft_ispath.c\
	  ft_lst_dysplay.c\
	  ft_dysplay_l_option.c\
	  ft_lst_mode_l.c\
	  ft_bytes.c\
	  ft_lst_tri_int.c\
	  ft_tri_arg.c\
	  ft_init_stats.c\
	  ft_add_file.c



SRCS = $(addprefix sources/,$(SRC))

OBJ = $(SRCS:.c=.o)

$(NAME) : $(OBJ)
	(cd $(LIB_DIR) && $(MAKE))
	ar -r $(NAME_LIB) $(OBJ)
	gcc -o $(NAME) $(CFLAGS) $(LIB_DIR)/libft.a $(NAME_LIB)

all : $(NAME)

clean :
	(cd $(LIB_DIR) && make clean && cd ..)
	rm -rf $(OBJ)

fclean : clean
	(cd $(LIB_DIR) && make fclean && cd ..)
	rm -rf $(NAME)
	rm -rf $(NAME_LIB)

re : fclean all

.PHONY: all clean flcean re
