/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdysplay.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/11 17:28:15 by abary             #+#    #+#             */
/*   Updated: 2016/02/11 17:32:57 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdysplay(t_list **begin)
{
	t_list	*lst;
	lst = *begin;

	while (lst)
	{
		ft_putendl(lst->content);
		lst = lst->next;
	}
}
