/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/06 15:20:02 by abary             #+#    #+#             */
/*   Updated: 2016/02/17 20:08:57 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LS_H
# define LS_H
# define MY_OPTIONS 6
# define DEFAULT 0
# define STAT 1
# define R 2
# define A 3
# define INVERSE 4
# define TIME 5
# define BUFF_LINK 1024
# include <dirent.h>
# include <sys/stat.h>
# include <string.h>

unsigned char	g_erreur;
/*
*******************************************************************************
**								Structures									  *
*******************************************************************************
*/
/*
**	Options Struct
*/

typedef struct	s_options
{
	char					option;
	char					boolean;
}				t_options;
typedef struct	s_directory
{
	char					*file;
	char					*dir;
	unsigned char			type;
	unsigned short			len;
	unsigned int			user_id;
	unsigned int			grp_id;
	char					mode;
	char					ruser;
	char					wuser;
	char					xuser;
	char					rgrp;
	char					wgrp;
	char					xgrp;
	char					roth;
	char					woth;
	char					xoth;
	unsigned int			nbytes;
	int						spacebytes;
	unsigned int			link;
	long					temps;
	int						spacelink;
	char					*useruid;
	size_t					lenuser;
	char					*grpuid;
	char					*date;
	unsigned int			major;
	unsigned int			minor;
	int						spacemajor;
	int						spaceminor;
	size_t					lengrp;
	unsigned int			blocks;
	char					*slink;
	void					*next;
}				t_directory;
typedef	struct	s_space
{
	int						spacelink;
	int						spaceusr;
	int						spacegrp;
	int						spacebytes;
	int						spacemajor;
	int						spaceminor;
	unsigned int			total;
}				t_space;
struct stat		*ft_init_stats(char *dir, char *path);
t_options		*ft_create_options(t_options *options);
t_directory		*ft_lst_newdir(struct dirent *dir, char *directory,
		t_options *options);
t_directory		*ft_rights_user(struct stat mystat, t_directory *directory);
void			ft_lst_del(t_directory *lst, t_options *options);
t_directory		*ft_rights_grp(struct stat mystat, t_directory *directory);
t_directory		*ft_rights_oth(struct stat mystat, t_directory *directory);
t_directory		*ft_bytes(struct stat mystat, t_directory *directory);
t_directory		*ft_time(struct stat mystat, t_directory *directory);
t_directory		*ft_devices(struct stat mystat, t_directory *directory);
t_directory		*ft_puid(t_directory *directory);
t_directory		*ft_add_file(char *str, t_options *options,
		t_directory *directory, int dir);
t_directory		*ft_options_l(t_directory *directory, char *dir, char *path);
t_directory		*ft_options_t(t_directory *directory, char *dir, char *path);
void			ft_lst_addtri_intinv(t_directory **alst, t_directory *new);
void			ft_lst_addtri_intnor(t_directory **alst, t_directory *new);
void			ft_lst_space(t_space *space, t_directory *new,
				t_directory **alst);
char			ft_mode_l(struct stat mystat);
int				ft_mode_type(struct stat mystat);
void			ft_lst_addtri(t_directory **alst, t_directory *new, t_space
		*space, t_options *options);
/*
******************************************************************************
**								LS											  *
*******************************************************************************
*/
void			ft_ls(int argc, char **argv, t_options *options);
/*
*******************************************************************************
**			                   Error management							      *
*******************************************************************************
*/

/*
**	Erreurs sur les options
*/
int				ft_valid_options(char **argv, int *argc, t_options *options);
void			ft_display_valid_options(t_options *options, int arg,
		char **argv);
void			ft_ls_erreur(char *dir, int erreur);
void			ft_check_input_error(char **arg, int i, int argc);
/*
*******************************************************************************
**								Read functions								  *
*******************************************************************************
*/
void			ft_read_dir(char *dir, t_options *options, t_space *space);
/*
*******************************************************************************
**								Dysplay										  *
*******************************************************************************
*/

void			ft_lst_dysplay_file(t_directory **begin, t_options *options,
		t_space *space, int total);
int				ft_dysplay_dir(char *str, int multiarg, int ligne);
int				ft_dysplay_arg(char **str, int i, int argc, t_options *options);
void			ft_dysplay_l_option(t_directory *directory, t_space *space);
/*
*******************************************************************************
**								OTHERS										  *
*******************************************************************************
*/
int				ft_isnopath(char *str);
void			ft_tri_arg(int i, int argc, char **argv, t_options *options);
#endif
