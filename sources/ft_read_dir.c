/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_read_dir.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/07 19:52:58 by abary             #+#    #+#             */
/*   Updated: 2016/02/17 19:44:57 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <dirent.h>
#include <stdlib.h>
#include "libft.h"
#include "ls.h"
#include <sys/errno.h>
#include <sys/dir.h>
#include <sys/types.h>

static char		*ft_create_path(char *directory)
{
	char	*s;

	s = directory;
	if (*(directory + ft_strlen(directory) - 1) != '/')
		directory = ft_strjoin(directory, "/");
	else
		directory = ft_strdup(directory);
	return (directory);
}

void			ft_lst_del(t_directory *lst, t_options *options)
{
	if (lst->next)
		ft_lst_del(lst->next, options);
	free(lst->file);
	if (lst->dir)
		free(lst->dir);
	if (options[STAT].boolean)
	{
		if (lst->useruid)
		{
			free(lst->useruid);
			free(lst->grpuid);
			free(lst->date);
		}
		if (lst->slink)
			free(lst->slink);
	}
	free(lst);
}

static	void	ft_recursion(t_directory **begin, t_options
		*options, t_space *space)
{
	t_directory *lst;

	lst = *begin;
	if (lst)
	{
		while (lst)
		{
			if (lst->dir)
			{
				ft_printf("\n%s:\n", lst->dir);
				ft_read_dir(lst->dir, options, space);
			}
			lst = lst->next;
		}
	}
}

static void		ft_clear(char *directory, t_directory *begin, DIR *dir,
		t_options *options)
{
	if (directory)
		free(directory);
	if (begin)
		ft_lst_del(begin, options);
	closedir(dir);
}

void			ft_read_dir(char *directory, t_options *o, t_space *space)
{
	DIR				*dir;
	struct dirent	*read_d;
	t_directory		*lst_dir;
	t_directory		*begin;
	int				ok;

	begin = NULL;
	ok = 0;
	if (!(dir = opendir(directory)))
		ft_ls_erreur(directory, errno);
	else
	{
		directory = ft_create_path(directory);
		while ((read_d = readdir(dir)) != NULL)
		{
			if ((o[A].boolean) || (!o[A].boolean && *(read_d->d_name) != '.'))
				if ((lst_dir = ft_lst_newdir(read_d, directory, o)) && ++ok)
					ft_lst_addtri(&begin, lst_dir, space, o);
		}
		ft_lst_dysplay_file(&begin, o, space, ok);
		if (o[R].boolean)
			ft_recursion(&begin, o, space);
		ft_clear(directory, begin, dir, o);
	}
}
