/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst__newdir.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/10 14:44:13 by abary             #+#    #+#             */
/*   Updated: 2016/02/17 20:09:34 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <dirent.h>
#include <sys/stat.h>
#include <ls.h>
#include "libft.h"
#include <stdlib.h>
#include <unistd.h>

t_directory			*ft_options_t(t_directory *directory, char *dir, char *path)
{
	struct stat	*mystat;

	if ((mystat = ft_init_stats(dir, path)))
	{
		directory->temps = mystat->st_mtimespec.tv_sec;
		if (mystat)
			free(mystat);
	}
	return (directory);
}

static t_directory	*ft_fil_dir(t_directory *directory, struct stat *mystat)
{
	directory->mode = mystat->st_mode;
	directory->user_id = mystat->st_uid;
	directory->grp_id = mystat->st_gid;
	directory->blocks = mystat->st_blocks;
	if ((directory->mode = ft_mode_l(*mystat)) == 'c')
		ft_devices(*mystat, directory);
	directory = ft_rights_user(*mystat, directory);
	directory = ft_rights_grp(*mystat, directory);
	directory = ft_rights_oth(*mystat, directory);
	directory = ft_bytes(*mystat, directory);
	directory = ft_time(*mystat, directory);
	directory = ft_puid(directory);
	return (directory);
}

t_directory			*ft_options_l(t_directory *directory, char *dir, char *path)
{
	struct stat	*mystat;
	char		buf[BUFF_LINK];
	int			size;

	mystat = ft_init_stats(dir, path);
	if (mystat)
	{
		size = readlink(path, buf, BUFF_LINK);
		if (size == -1)
			directory->slink = NULL;
		else
		{
			buf[size] = '\0';
			directory->slink = ft_strdup(buf);
		}
		directory = ft_fil_dir(directory, mystat);
	}
	else
		return (NULL);
	if (mystat)
		free(mystat);
	return (directory);
}

t_directory			*ft_lst_newdir_name(struct dirent *dir, char *path)
{
	struct stat	*mystat;
	t_directory *directory;

	directory = (t_directory *)ft_memalloc(sizeof(t_directory));
	if (!directory)
		return (NULL);
	if ((mystat = ft_init_stats(path, NULL)))
	{
		directory->type = ft_mode_type(*mystat);
		free(mystat);
	}
	else
		directory->type = 0;
	directory->len = dir->d_namlen;
	directory->file = ft_strdup(dir->d_name);
	return (directory);
}

t_directory			*ft_lst_newdir(struct dirent *dir, char *path,
		t_options *options)
{
	t_directory *directory;
	char		*pathfile;
	char		*file;

	pathfile = ft_strjoin(path, dir->d_name);
	directory = ft_lst_newdir_name(dir, pathfile);
	file = directory->file;
	if (directory->type == 4 && ft_isnopath((char *)dir->d_name))
		directory->dir = ft_strjoin(path, dir->d_name);
	directory->next = NULL;
	if (options[STAT].boolean)
	{
		if (!(ft_options_l(directory, directory->dir, pathfile)))
		{
			free(directory);
			return (NULL);
		}
	}
	if (options[TIME].boolean)
		ft_options_t(directory, directory->dir, pathfile);
	free(pathfile);
	return (directory);
}
