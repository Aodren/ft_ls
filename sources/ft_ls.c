/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/06 15:32:04 by abary             #+#    #+#             */
/*   Updated: 2016/02/17 19:57:11 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ls.h"
#include <stdlib.h>

static	int	ft_while_arg(int argc, char **argv)
{
	int i;

	i = 1;
	while (i < argc && *(*(argv + i)) == '-' && *(*(argv + i) + 1) != '-')
	{
		if (*(*(argv + i)) == '-' && *(*(argv + i) + 1) == '\0')
			break ;
		++i;
	}
	if (i < argc && **(argv + i) == '-' && *(*(argv + i) + 1) == '-')
		++i;
	return (i);
}

void		ft_ls(int argc, char **argv, t_options *options)
{
	int			i;
	int			multiarg;
	int			ligne;
	t_space		*space;

	multiarg = 0;
	space = ft_memalloc(sizeof(t_space));
	i = ft_while_arg(argc, argv);
	if (i == argc)
		ft_read_dir(".", options, space);
	ft_check_input_error(argv, i, argc);
	ligne = ft_dysplay_arg(argv, i, argc, options);
	if (argc - i > 1)
		multiarg = 1;
	ft_tri_arg(i, argc, argv, options);
	while (++i <= argc)
	{
		if (*(argv + i - 1))
		{
			ligne = ft_dysplay_dir(*(argv + i - 1), multiarg, ligne);
			ft_read_dir(*(argv + i - 1), options, space);
		}
	}
	free(space);
}
