/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dysplay_l_option.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/11 21:58:47 by abary             #+#    #+#             */
/*   Updated: 2016/02/17 20:02:02 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"
#include "libft.h"
#include <sys/stat.h>
#include <stdlib.h>

static void	ft_print_rights(t_directory *directory)
{
	ft_putchar(directory->mode);
	ft_putchar(directory->ruser);
	ft_putchar(directory->wuser);
	ft_putchar(directory->xuser);
	ft_putchar(directory->rgrp);
	ft_putchar(directory->wgrp);
	ft_putchar(directory->xgrp);
	ft_putchar(directory->roth);
	ft_putchar(directory->woth);
	ft_putchar(directory->xoth);
}

static void	ft_display_l_option1(t_directory *directory, t_space *space)
{
	int s;

	s = space->spacelink - directory->spacelink;
	while (--s >= -2)
		ft_putchar(' ');
	ft_putnbr(directory->link);
	ft_putchar(' ');
	ft_putstr(directory->useruid);
	s = space->spaceusr - ft_strlen(directory->useruid);
	while (--s >= -2)
		ft_putchar(' ');
	ft_putstr(directory->grpuid);
	s = space->spacegrp - ft_strlen(directory->grpuid);
	while (--s >= -1)
		ft_putchar(' ');
	s = space->spacebytes - directory->spacebytes;
	while (--s >= -1)
		ft_putchar(' ');
}

static void	ft_dysplay_l_optionc(t_directory *directory, t_space *space)
{
	int s;

	s = space->spacemajor - directory->spacemajor;
	while (--s >= -2)
		ft_putchar(' ');
	if (directory->major)
		ft_putnbr(directory->major);
	else
		ft_putstr("  ");
	s = space->spaceminor - directory->spaceminor;
	while (--s >= -3)
		ft_putchar(' ');
	ft_putnbr(directory->minor);
}

void		ft_dysplay_l_option(t_directory *directory, t_space *space)
{
	ft_print_rights(directory);
	ft_display_l_option1(directory, space);
	if (directory->mode == 'c')
		ft_dysplay_l_optionc(directory, space);
	else
		ft_putnbr(directory->nbytes);
	ft_putchar(' ');
	ft_putstr(directory->date);
	ft_putchar(' ');
	ft_putstr(directory->file);
	if (directory->slink)
	{
		ft_putstr(" -> ");
		ft_putstr(directory->slink);
	}
}
