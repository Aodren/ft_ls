/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bytes.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 16:56:19 by abary             #+#    #+#             */
/*   Updated: 2016/02/17 20:02:38 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"
#include "libft.h"
#include <time.h>
#include <pwd.h>
#include <grp.h>
#include <stdlib.h>
#include <sys/types.h>

t_directory		*ft_devices(struct stat mystat, t_directory *directory)
{
	unsigned int minor;
	unsigned int major;

	major = major(mystat.st_rdev);
	minor = minor(mystat.st_rdev);
	directory->minor = minor;
	directory->major = major;
	directory->spaceminor = ft_size_int(minor);
	directory->spacemajor = ft_size_int(major);
	return (directory);
}

t_directory		*ft_bytes(struct stat mystat, t_directory *directory)
{
	unsigned int i;

	i = mystat.st_size;
	directory->nbytes = i;
	directory->spacebytes = ft_size_int(i);
	i = mystat.st_nlink;
	directory->link = i;
	directory->spacelink = ft_size_int(i);
	return (directory);
}

t_directory		*ft_puid(t_directory *directory)
{
	struct passwd	*mesuid;
	struct group	*mygrps;

	mesuid = getpwuid(directory->user_id);
	directory->useruid = ft_strdup(mesuid->pw_name);
	if ((mygrps = getgrgid(directory->grp_id)))
		directory->grpuid = ft_strdup(mygrps->gr_name);
	else
		directory->grpuid = ft_strdup("101");
	return (directory);
}

static	char	*ft_while_date(char *date)
{
	while (*date)
	{
		if (*date == '\n')
			*date = '\0';
		++date;
	}
	while (*date != ' ')
		--date;
	return (date);
}

t_directory		*ft_time(struct stat mystat, t_directory *directory)
{
	time_t			t;
	char			*date;
	char			*liberte;
	int				space;

	t = time(NULL);
	date = ctime((const time_t*)&(mystat.st_mtime));
	space = 0;
	if (t - mystat.st_mtimespec.tv_sec > 15776999
			|| t - mystat.st_mtimespec.tv_sec < 0)
	{
		directory->date = ft_strndup(date + 4, 6);
		liberte = directory->date;
		directory->date = ft_strjoin(directory->date, " ");
		free(liberte);
		liberte = directory->date;
		date = ft_while_date(date);
		directory->date = ft_strjoin(directory->date, date);
		free(liberte);
	}
	else
		directory->date = ft_strndup(date + 4, 12);
	return (directory);
}
