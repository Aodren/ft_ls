/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tri_arg.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/16 12:50:24 by abary             #+#    #+#             */
/*   Updated: 2016/02/17 19:45:23 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ls.h"

static void	ft_tri_arg_nor(int i, int argc, char **argv)
{
	int		ok;
	int		save;
	char	*swap;

	ok = 1;
	while (ok)
	{
		ok = 0;
		save = i;
		while (save < argc)
		{
			if (*(argv + save) && *(argv + save + 1) &&
					ft_strcmp(argv[save], argv[save + 1]) > 0)
			{
				swap = *(argv + save);
				*(argv + save) = *(argv + save + 1);
				*(argv + save + 1) = swap;
				ok = 1;
			}
			++save;
		}
	}
}

static void	ft_tri_arg_inv(int i, int argc, char **argv)
{
	int		ok;
	int		save;
	char	*swap;

	ok = 1;
	while (ok)
	{
		ok = 0;
		save = i;
		while (save < argc)
		{
			if (*(argv + save) && *(argv + save + 1) &&
					ft_strcmp(argv[save], argv[save + 1]) < 0)
			{
				swap = *(argv + save);
				*(argv + save) = *(argv + save + 1);
				*(argv + save + 1) = swap;
				ok = 1;
			}
			++save;
		}
	}
}

void		ft_tri_arg(int i, int argc, char **argv, t_options *options)
{
	if (options[INVERSE].boolean)
		ft_tri_arg_inv(i, argc, argv);
	else
		ft_tri_arg_nor(i, argc, argv);
}
