/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls_erreur.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/10 14:16:49 by abary             #+#    #+#             */
/*   Updated: 2016/02/17 20:07:13 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <dirent.h>
#include <stdlib.h>
#include <sys/errno.h>
#include "ls.h"

void		ft_ls_erreur(char *dir, int erreur)
{
	char	*str;

	g_erreur = 1;
	if (erreur != 20 && erreur != 2)
	{
		if ((str = strerror(erreur)))
			ft_print_error("ft_ls : %s: %s.\n", dir, str);
	}
}

void		ft_check_input_error(char **arg, int i, int argc)
{
	DIR				*dir;
	char			*str;
	struct stat		*mystat;

	str = NULL;
	while (i < argc)
	{
		dir = opendir(*(arg + i));
		if (dir)
			closedir(dir);
		else if (errno != 20 && errno != 13)
		{
			if ((mystat = ft_init_stats(*(arg + i), NULL)))
				free(mystat);
			else
			{
				g_erreur = 1;
				if ((str = strerror(errno)))
					ft_print_error("ft_ls: %s: %s\n", *(arg + i), str);
			}
		}
		++i;
	}
	if (str)
		free(str);
}
