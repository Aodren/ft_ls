/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_valid_options.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/07 15:07:16 by abary             #+#    #+#             */
/*   Updated: 2016/02/13 20:48:30 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"
#include "libft.h"
#include <stdlib.h>

static int			ft_fill_options(char *argv, t_options *options)
{
	int i;
	int ok;

	while (*argv)
	{
		i = -1;
		ok = 0;
		while (++i < MY_OPTIONS)
		{
			if (*argv == options[i].option)
			{
				options[i].boolean = 1;
				ok = 1;
				break ;
			}
		}
		if (!ok)
			return (ok);
		++argv;
	}
	return (ok);
}

static void			init_p_options(t_options *options)
{
	int i;

	i = -1;
	while (++i < MY_OPTIONS)
		options[i].boolean = 0;
}

int					ft_valid_options(char **argv,
		int *argc, t_options *options)
{
	init_p_options(options);
	while (--*argc)
	{
		if (**argv == '-' && *(*argv + 1) == '-' && *(*argv + 2) == '\0')
			return (1);
		if (**argv == '-' && *(*argv + 1))
		{
			if (!ft_fill_options(*argv + 1, options))
				return (0);
		}
		else
			return (1);
		++argv;
	}
	return (1);
}
