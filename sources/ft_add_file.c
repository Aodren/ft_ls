/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_add_file.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/17 17:36:18 by abary             #+#    #+#             */
/*   Updated: 2016/02/17 20:11:15 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"
#include "libft.h"
#include <unistd.h>
#include <stdlib.h>

static void	ft_add_file_free(t_directory *directory)
{
	free(directory->file);
	free(directory->dir);
	if (directory->slink)
		free(directory->slink);
	free(directory);
}

t_directory	*ft_add_file_init(t_directory *directory, char *str)
{
	directory = (t_directory *)ft_memalloc(sizeof(t_directory));
	directory->slink = NULL;
	directory->file = ft_strdup(str);
	directory->dir = ft_strdup(str);
	if ((directory = ft_options_l(directory, directory->file, NULL)))
		directory = ft_options_t(directory, directory->file, NULL);
	return (directory);
}

t_directory	*ft_add_file(char *str, t_options *options,
		t_directory *directory, int dir)
{
	char	buf[BUFF_LINK];
	int		size;

	size = readlink(str, buf, BUFF_LINK);
	directory = ft_add_file_init(directory, str);
	if (size != -1 && !(buf[size] = 0))
		directory->slink = ft_strdup(buf);
	if (dir == 1)
	{
		if (!directory->slink)
		{
			ft_add_file_free(directory);
			return (NULL);
		}
		else if (options[DEFAULT].boolean && directory->slink)
		{
			ft_add_file_free(directory);
			str = NULL;
			return (NULL);
		}
	}
	return (directory);
}
