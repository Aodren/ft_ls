/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst_tri_int.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/14 13:55:55 by abary             #+#    #+#             */
/*   Updated: 2016/02/17 20:03:45 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"
#include "libft.h"

void		ft_lst_addtri_intnor(t_directory **alst, t_directory *new)
{
	t_directory *lst;
	t_directory *tmp;

	lst = *alst;
	tmp = NULL;
	if (lst)
	{
		while (lst && (lst->temps > new->temps || (lst->temps == new->temps &&
						ft_strcmp(lst->file, new->file) < 0)))
		{
			tmp = lst;
			lst = lst->next;
		}
		new->next = lst;
		if (!tmp)
			*alst = new;
		else
			tmp->next = new;
	}
	else
		*alst = new;
}

void		ft_lst_addtri_intinv(t_directory **alst, t_directory *new)
{
	t_directory *lst;
	t_directory *tmp;

	lst = *alst;
	tmp = NULL;
	if (lst)
	{
		while (lst && lst->temps < new->temps)
		{
			tmp = lst;
			lst = lst->next;
		}
		new->next = lst;
		if (!tmp)
			*alst = new;
		else
			tmp->next = new;
	}
	else
		*alst = new;
}
