/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_stats.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/17 17:30:25 by abary             #+#    #+#             */
/*   Updated: 2016/02/17 20:04:36 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/stat.h>
#include "libft.h"
#include <stdlib.h>

struct stat	*ft_init_stats(char *dir, char *path)
{
	struct stat	*mystat;
	int			lst;

	mystat = NULL;
	mystat = (struct stat *)ft_memalloc(sizeof(struct stat));
	if (dir)
		lst = lstat(dir, mystat);
	else
		lst = lstat(path, mystat);
	if (lst == -1)
	{
		free(mystat);
		return (NULL);
	}
	return (mystat);
}
