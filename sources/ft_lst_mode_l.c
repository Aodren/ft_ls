/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst_mode_l.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/12 14:51:50 by abary             #+#    #+#             */
/*   Updated: 2016/02/17 19:45:46 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"
#include <sys/stat.h>

t_directory	*ft_rights_user(struct stat mystat, t_directory *directory)
{
	int nbr;
	int ok;

	ok = 0;
	nbr = mystat.st_mode;
	if ((nbr & S_IRUSR) == S_IRUSR && (ok = 1))
		directory->ruser = 'r';
	else
		directory->ruser = '-';
	if ((nbr & S_IWUSR) == S_IWUSR && (ok = 1))
		directory->wuser = 'w';
	else
		directory->wuser = '-';
	if ((nbr & S_ISUID) == S_ISUID)
	{
		if ((nbr & S_IRWXG) == S_IRWXG || (nbr & S_IRWXO) == S_IRWXO || ok)
			directory->xuser = 's';
		else
			directory->xuser = 'S';
	}
	else if ((nbr & S_IXUSR) == S_IXUSR)
		directory->xuser = 'x';
	else
		directory->xuser = '-';
	return (directory);
}

t_directory	*ft_rights_grp(struct stat mystat, t_directory *directory)
{
	int nbr;
	int ok;

	ok = 0;
	nbr = mystat.st_mode;
	if ((nbr & S_IRGRP) == S_IRGRP && (ok = 1))
		directory->rgrp = 'r';
	else
		directory->rgrp = '-';
	if ((nbr & S_IWGRP) == S_IWGRP && (ok = 1))
		directory->wgrp = 'w';
	else
		directory->wgrp = '-';
	if ((nbr & S_ISGID) == S_ISGID)
	{
		if ((nbr & S_IRWXU) != S_IRWXU && (nbr & S_IRWXO) != S_IRWXO && !ok)
			directory->xgrp = 'S';
		else
			directory->xgrp = 's';
	}
	else if ((nbr & S_IXGRP) == S_IXGRP)
		directory->xgrp = 'x';
	else
		directory->xgrp = '-';
	return (directory);
}

t_directory	*ft_rights_oth(struct stat mystat, t_directory *directory)
{
	int nbr;
	int ok;

	ok = 0;
	nbr = mystat.st_mode;
	if ((nbr & S_IROTH) == S_IROTH && (ok = 1))
		directory->roth = 'r';
	else
		directory->roth = '-';
	if ((nbr & S_IWOTH) == S_IWOTH && (ok = 1))
		directory->woth = 'w';
	else
		directory->woth = '-';
	if ((nbr & S_ISVTX) == S_ISVTX)
	{
		if ((nbr & S_IRWXU) != S_IRWXU && (nbr & S_IRWXG) != S_IRWXG && !ok)
			directory->xoth = 'T';
		else
			directory->xoth = 't';
	}
	else if ((nbr & S_IXOTH) == S_IXOTH)
		directory->xoth = 'x';
	else
		directory->xoth = '-';
	return (directory);
}

char		ft_mode_l(struct stat mystat)
{
	if ((mystat.st_mode & S_IFLNK) == S_IFLNK)
		return ('l');
	else if ((mystat.st_mode & S_IFBLK) == S_IFBLK)
		return ('b');
	else if ((mystat.st_mode & S_IFCHR) == S_IFCHR)
		return ('c');
	else if ((mystat.st_mode & S_IFSOCK) == S_IFSOCK)
		return ('s');
	else if ((mystat.st_mode & S_IFIFO) == S_IFIFO)
		return ('p');
	else if ((mystat.st_mode & S_IFDIR) == S_IFDIR)
		return ('d');
	return ('-');
}

int			ft_mode_type(struct stat mystat)
{
	if ((mystat.st_mode & S_IFLNK) == S_IFLNK)
		return (10);
	else if ((mystat.st_mode & S_IFBLK) == S_IFBLK)
		return (6);
	else if ((mystat.st_mode & S_IFCHR) == S_IFCHR)
		return (2);
	else if ((mystat.st_mode & S_IFSOCK) == S_IFSOCK)
		return (12);
	else if ((mystat.st_mode & S_IFIFO) == S_IFIFO)
		return (1);
	else if ((mystat.st_mode & S_IFDIR) == S_IFDIR)
		return (4);
	return (0);
}
