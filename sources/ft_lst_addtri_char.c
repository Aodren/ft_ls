/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst_addtri_char.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/09 15:16:12 by abary             #+#    #+#             */
/*   Updated: 2016/02/17 20:04:14 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ls.h"

void			ft_lst_space(t_space *space, t_directory *new,
		t_directory **alst)
{
	if (!*alst)
	{
		space->total = 0;
		space->spacelink = new->spacelink;
		space->spacebytes = new->spacebytes;
		space->spaceusr = ft_strlen(new->useruid);
		space->spacegrp = ft_strlen(new->grpuid);
		space->spacemajor = new->spacemajor;
		space->spaceminor = new->spaceminor;
		space->total += new->blocks;
	}
	else
	{
		space->total += new->blocks;
		if (new->spacelink > space->spacelink)
			space->spacelink = new->spacelink;
		if (new->spacebytes > space->spacebytes)
			space->spacebytes = new->spacebytes;
		if ((int)ft_strlen(new->useruid) > space->spaceusr)
			space->spaceusr = ft_strlen(new->useruid);
		if ((int)ft_strlen(new->grpuid) > space->spacegrp)
			space->spacegrp = ft_strlen(new->grpuid);
	}
}

static void		ft_lst_addtri_inv(t_directory **alst, t_directory *new)
{
	t_directory *lst;
	t_directory *tmp;

	lst = *alst;
	tmp = NULL;
	if (lst)
	{
		while (lst && ft_strcmp(lst->file, new->file) > 0)
		{
			tmp = lst;
			lst = lst->next;
		}
		new->next = lst;
		if (!tmp)
			*alst = new;
		else
			tmp->next = new;
	}
	else
		*alst = new;
}

static void		ft_lst_addtri_nor(t_directory **alst, t_directory *new)
{
	t_directory *lst;
	t_directory *tmp;

	lst = *alst;
	tmp = NULL;
	if (lst)
	{
		while (lst && ft_strcmp(lst->file, new->file) < 0)
		{
			tmp = lst;
			lst = lst->next;
		}
		new->next = lst;
		if (!tmp)
			*alst = new;
		else
			tmp->next = new;
	}
	else
		*alst = new;
}

void			ft_lst_addtri(t_directory **alst, t_directory *new,
		t_space *space, t_options *options)
{
	if (options[STAT].boolean)
		ft_lst_space(space, new, alst);
	if (options[INVERSE].boolean)
	{
		if (options[TIME].boolean)
			ft_lst_addtri_intinv(alst, new);
		else
			ft_lst_addtri_inv(alst, new);
	}
	else
	{
		if (options[TIME].boolean)
			ft_lst_addtri_intnor(alst, new);
		else
			ft_lst_addtri_nor(alst, new);
	}
}
