/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_valid_options.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/07 17:37:17 by abary             #+#    #+#             */
/*   Updated: 2016/02/17 19:32:03 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"
#include "libft.h"
#include <sys/errno.h>
#include <dirent.h>
#include <stdlib.h>
#include <unistd.h>

void				ft_display_valid_options(t_options *options, int ac,
		char **argv)
{
	int i;
	int ok;

	*(argv + ac) = *(argv + ac) + 1;
	while (**(argv + ac))
	{
		i = -1;
		ok = 0;
		while (++i < MY_OPTIONS)
		{
			if (**(argv + ac) == options[i].option)
				ok = 1;
		}
		if (!ok)
		{
			ft_print_error("ft_ls: illegal option -- %c\n", **(argv + ac));
			ft_print_error("usage: ft_ls [-Ralrt1] [file ...]\n");
			break ;
		}
		++*(argv + ac);
	}
}

int					ft_dysplay_dir(char *str, int multiarg, int ligne)
{
	DIR *dir;

	if (multiarg)
	{
		dir = opendir(str);
		if (dir)
		{
			if (ligne)
				ft_printf("\n%s:\n", str);
			else
			{
				ligne = 1;
				ft_printf("%s:\n", str);
			}
			closedir(dir);
		}
	}
	return (ligne);
}

static void			ft_end(t_directory *begin, t_options *options,
		t_space *space)
{
	if (begin)
	{
		ft_lst_dysplay_file(&begin, options, space, 0);
		ft_lst_del(begin, options);
	}
	free(space);
}

static t_directory	*ft_norm(char **str, t_directory *lst, t_options *options,
		int i)
{
	if (*(*(str + i) + ft_strlen(*(str + i)) - 1) != '/')
	{
		if ((lst = ft_add_file(*(str + i), options, lst, 1)))
		{
			*(str + i) = NULL;
			return (lst);
		}
	}
	return (NULL);
}

int					ft_dysplay_arg(char **str, int i, int argc,
		t_options *options)
{
	DIR			*dir;
	int			vrai;
	t_directory *lst;
	t_directory *begin;
	t_space		*space;

	space = (t_space *)ft_memalloc(sizeof(t_space));
	vrai = 0;
	begin = NULL;
	while (i < argc)
	{
		if ((dir = opendir(*(str + i))))
		{
			if ((lst = ft_norm(str, lst, options, i)) && ++vrai)
				ft_lst_addtri(&begin, lst, space, options);
			closedir(dir);
		}
		else if ((lst = ft_add_file(*(str + i), options, lst, 20)) && ++vrai)
			ft_lst_addtri(&begin, lst, space, options);
		++i;
	}
	ft_end(begin, options, space);
	return (vrai);
}
