/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/06 15:21:33 by abary             #+#    #+#             */
/*   Updated: 2016/02/17 19:53:57 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"
#include "libft.h"
#include <stdlib.h>

int		main(int argc, char **argv)
{
	t_options	*options;
	int			ac;

	g_erreur = 0;
	ac = argc;
	options = NULL;
	options = ft_create_options(options);
	if (ft_valid_options(argv + 1, &argc, options))
		ft_ls(ac, argv, options);
	else
	{
		g_erreur = 1;
		ft_display_valid_options(options, ac - argc, argv);
	}
	free(options);
	return (g_erreur);
}
