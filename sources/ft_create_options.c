/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_options.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/07 17:30:38 by abary             #+#    #+#             */
/*   Updated: 2016/02/14 13:40:07 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"
#include "libft.h"

t_options	*ft_create_options(t_options *options)
{
	options = (t_options *)ft_memalloc(sizeof(t_options) * MY_OPTIONS);
	options[0].option = '1';
	options[1].option = 'l';
	options[2].option = 'R';
	options[3].option = 'a';
	options[4].option = 'r';
	options[5].option = 't';
	return (options);
}
