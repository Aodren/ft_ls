/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst_dysplay.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/10 18:38:46 by abary             #+#    #+#             */
/*   Updated: 2016/02/17 19:35:20 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ls.h"
#include "libft.h"
#include <dirent.h>

void	ft_lst_dysplay_file(t_directory **begin, t_options *options,
		t_space *space, int total)
{
	t_directory *lst;

	lst = *begin;
	if (options[STAT].boolean && total)
		ft_printf("total %d\n", space->total);
	while (lst)
	{
		if (options[STAT].boolean)
		{
			ft_dysplay_l_option(lst, space);
			ft_putchar('\n');
		}
		else
			ft_putendl(lst->file);
		lst = lst->next;
	}
}
